# [1.2.0](https://gitlab.com/to-be-continuous/dbt/compare/1.1.1...1.2.0) (2023-01-28)


### Features

* **gcp:** add gcp-auth-provider variant ([441531a](https://gitlab.com/to-be-continuous/dbt/commit/441531a299b22ecce91c0c7cf9741c490f63f860))

## [1.1.1](https://gitlab.com/to-be-continuous/dbt/compare/1.1.0...1.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([1526977](https://gitlab.com/to-be-continuous/dbt/commit/1526977d16acceaec62c875da670dd2e64216121))

# [1.1.0](https://gitlab.com/to-be-continuous/dbt/compare/1.0.1...1.1.0) (2022-09-07)


### Features

* add SQLFluff for dbt project ([a91edfa](https://gitlab.com/to-be-continuous/dbt/commit/a91edfa6530060c30197fe19f53e9220bf7da2fe))

## [1.0.1](https://gitlab.com/to-be-continuous/dbt/compare/1.0.0...1.0.1) (2022-08-16)


### Bug Fixes

* add assert on required dbt_adapter ([d249a58](https://gitlab.com/to-be-continuous/dbt/commit/d249a58c9c67550221a48ed82b8050d3d58ce022))

# 1.0.0 (2022-08-16)


### Features

* initialize dbt template ([b37b79c](https://gitlab.com/to-be-continuous/dbt/commit/b37b79c5a97a191fcbfee77455bfb3c9dff69323))
