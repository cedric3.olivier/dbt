# GitLab CI template for dbt

This project implements a GitLab CI/CD template to continuously integrate and deploy your data with [dbt](https://www.getdbt.com/)

### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/dbt'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-dbt.yml'
```

You can find a sample of dbt project here : https://gitlab.com/to-be-continuous/samples/dbt-sample/

## Global configuration

The dbt template uses some global configuration used throughout all jobs.

| Name                  | description                            | default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `DBT_IMAGE`           | The Docker image used to run dbt       | `registry.hub.docker.com/library/python:latest`   |
| `DBT_PROJECT_DIR`     | The [dbt_project.yml](https://docs.getdbt.com/reference/dbt_project.yml) dir       | `.`   |
| `DBT_PROFILES_DIR`    | The dbt [profile](https://docs.getdbt.com/dbt-cli/configure-your-profile) location       | `.`   |
| `DBT_ADAPTER`         | The dbt [adapter](https://docs.getdbt.com/docs/available-adapters) to use       | __none__ (required)  |
| `DBT_TARGET`          | The dbt [target](https://docs.getdbt.com/reference/dbt-jinja-functions/target) to use  |  __none__ (required)  |
| `DBT_BUILD_ARGS`      | Arguments used by [`dbt cli`](https://docs.getdbt.com/reference/global-configs#command-line-flags)      | __none__          |

### Environments configuration

As seen above, the dbt template may support up to 4 environments (`review`, `integration`, `staging` and `production`).

Each deployment job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

* `environment_type`: set to the type of environment (`review`, `integration`, `staging` or `production`),
* `environment_name`: the application name (see below),

They may be freely used in downstream jobs (for instance to run acceptance tests against the latest deployed environment).

Here are configuration details for each environment.

#### Review environments

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

They are **disabled by default** and can be enabled by setting the `DBT_REVIEW_TARGET` variable (see below).

Here are variables supported to configure review environments:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_REVIEW_TARGET`      | dbt [target](https://docs.getdbt.com/reference/dbt-jinja-functions/target) for `review` env | _none_ (disabled) |


#### Integration environment

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `DBT_INTEG_TARGET` variable (see below).

Here are variables supported to configure the integration environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_INTEG_TARGET`       | dbt [target](https://docs.getdbt.com/reference/dbt-jinja-functions/target) for `integration` env | _none_ (disabled) |

#### Staging environment

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `DBT_STAGING_TARGET` variable (see below).

Here are variables supported to configure the staging environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_STAGING_TARGET`     | dbt [target](https://docs.getdbt.com/reference/dbt-jinja-functions/target) for `staging` env | _none_ (disabled) |

#### Production environment

The production environment is the final deployment environment associated with your production branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `DBT_PROD_TARGET` variable (see below).

Here are variables supported to configure the production environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_PROD_TARGET`        | dbt [target](https://docs.getdbt.com/reference/dbt-jinja-functions/target) for `production` env | _none_ (disabled) |


## Jobs

### `dbt-build` job

This job performs **build, doc generation** and documentation **coverage**.

`dbt-build` generates executable SQL from source model, test, and analysis files


### `dbt-sqlfluff-lint` job

This job performs **SQL Lint**.

`dbt-sqlfluff-lint` execute [sqlfluff linter with dbt plugin](https://pypi.org/project/sqlfluff-templater-dbt/) to lint SQL and uses the following variables:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_SQLFLUFF_ENABLED`   | set to `true` to enable SQLFluff lint  | _none_ (disabled) |
| `DBT_SQLFLUFF_LINT_ARGS` | Lint [options and arguments](https://docs.sqlfluff.com/en/stable/cli.html#sqlfluff-lint) | _none_ |

:warning: this jobs read [SQLFluff configuration files](https://docs.sqlfluff.com/en/stable/configuration.html) in `DBT_PROJECT_DIR` directory.

### `dbt-deploy` job

This job performs **deployment**.

`dbt-deploy` execute generated SQL from models on target and uses the following variables:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `DBT_DEPLOY_ENABLED`     | set to `true` to enable deployment     | _none_ (disabled) |

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).


## Variants

### GitLab Pages variant

Basically it copies the content of the dbt generated site folder (`target` by default) to the `public` folder which is published by [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works).

If you wish to use it, add the following to your `gitlab-ci.yml`:

```yaml
include:
  # main template
  - project: 'to-be-continuous/dbt'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-dbt.yml'
  # GitLab pages variant
  - project: 'to-be-continuous/dbt'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-dbt-pages.yml'
```

### Google Cloud variant

This variant allows retrieving an [OAuth access token](https://developers.google.com/identity/protocols/oauth2) for the [dbt BigQuery Adapter](https://docs.getdbt.com/reference/resource-configs/bigquery-configs) (using the [GCP Auth Provider](https://gitlab.com/to-be-continuous/tools/gcp-auth-provider) as a _service container_).

Provided you successfully configured the [federated authentication using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/), this variant automatically obtains a temporary OAuth token and stores it in the `$GOOGLE_OAUTH_ACCESS_TOKEN` variable (supported by the [dbt BigQuery Config Setup](https://docs.getdbt.com/reference/warehouse-setups/bigquery-setup#oauth-token-based) as an authentication credential).

#### Configuration

The variant requires the additional configuration parameters:

| Name              | description                            | default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_GCP_PROVIDER_IMAGE` | The [GCP Auth Provider](https://gitlab.com/to-be-continuous/tools/gcp-auth-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/gcp-auth-provider:main` |
| `GCP_OIDC_PROVIDER`      | Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) | _none_ |
| `GCP_OIDC_ACCOUNT`       | Default Service Account to which impersonate with OpenID Connect authentication | _none_ |
| `GCP_REVIEW_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment _(only define if different from default)_ | _none_ |
| `GCP_REVIEW_OIDC_ACCOUNT`  | Service Account to which impersonate with OpenID Connect authentication on `review` environment _(only define if different from default)_ | _none_ |
| `GCP_INTEG_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment _(only define if different from default)_ | _none_ |
| `GCP_INTEG_OIDC_ACCOUNT`  | Service Account to which impersonate with OpenID Connect authentication on `integration` environment _(only define if different from default)_ | _none_ |
| `GCP_STAGING_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment _(only define if different from default)_ | _none_ |
| `GCP_STAGING_OIDC_ACCOUNT`  | Service Account to which impersonate with OpenID Connect authentication on `staging` environment _(only define if different from default)_ | _none_ |
| `GCP_PROD_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment _(only define if different from default)_ | _none_ |
| `GCP_PROD_OIDC_ACCOUNT`  | Service Account to which impersonate with OpenID Connect authentication on `production` environment _(only define if different from default)_ | _none_ |

#### Example

With a common default `GCP_OIDC_PROVIDER` and `GCP_OIDC_ACCOUNT` configuration for non-prod environments, and a specific one for production:

```yaml
include:
  # main template
  - project: 'to-be-continuous/dbt'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-dbt.yml'
  # `Google Cloud` variant
  - project: 'to-be-continuous/dbt'
    ref: '1.2.0'
    file: '/templates/gitlab-ci-dbt-gcp.yml'

variables:
  # common OIDC config for non-prod envs
  GCP_OIDC_PROVIDER: "projects/<gcp_nonprod_proj_id>/locations/global/workloadIdentityPools/<pool_id>/providers/<provider_id>"
  GCP_OIDC_ACCOUNT: "<name>@$<gcp_nonprod_proj_id>.iam.gserviceaccount.com"
  # specific OIDC config for prod
  GCP_PROD_OIDC_PROVIDER: "projects/<gcp_prod_proj_id>/locations/global/workloadIdentityPools/<pool_id>/providers/<provider_id>"
  GCP_PROD_OIDC_ACCOUNT: "<name>@$<gcp_prod_proj_id>.iam.gserviceaccount.com"
```

To setup dbt to use `GOOGLE_OAUTH_ACCESS_TOKEN`, apply this configuration in dbt `profiles.yml`:

```yaml
my-bigquery-db:
  target: dev
  outputs:
    dev:
      type: bigquery
      method: oauth-secrets
      project: [GCP project id]
      dataset: [the name of your dbt dataset]
      threads: [1 or more]
      token: "{{ env_var('GOOGLE_OAUTH_ACCESS_TOKEN') }}"
      <optional_config>: <value>
```    


