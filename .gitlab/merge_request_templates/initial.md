## Presentation

(Select the type of template hereafter.)

Template type: **build** / **analyse** / **package** / **deploy** / **acceptance** / others ?

(Describe here the goal and context of this template.)


## Checklist

* General:
    * [ ] add project logo (`logo.png` file) - preferably 256x256
    * [ ] defines a base (hidden) job
    * [ ] use [rules](https://docs.gitlab.com/ee/ci/yaml/#rules) instead of [only/except](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced)
    * [ ] optimized [cache](https://docs.gitlab.com/ee/ci/caching/) configuration (wherever applicable)
* Publicly usable:
    * [ ] runners: untagged
    * [ ] no proxy configuration but support `http_proxy`/`https_proxy`/`no_proxy` configuration 
    * [ ] no custom CA certificate(s) but supports `$CUSTOM_CA_CERTS` or `$DEFAULT_CA_CERTS` to declare custom CA certificate(s)
    * [ ] internet hostnames/urls only
* Used Docker images:
    * [ ] **public** images
    * [ ] **official** images (when possible)
    * [ ] `latest` tag (when possible)
* Documented:
    * [ ] `README.md` presents the template, jobs, tools, variables and variants
    * [ ] `kicker.json` describes the template, jobs, tools, variables and variants
* Tested & examplified:
    * [ ] (url to a project sample successfully using this template)
    * [ ] (maybe another one with a different context)
    * ...

### Build & Test template checklist

(Remove this chapter if not applicable to your template type.)

* Build & Test job:
    * (type here the used build & test tools/frameworks)
    * [ ] mapped to the `build` stage
    * [ ] unit tests report integration using [JUnit test report](https://docs.gitlab.com/ee/ci/junit_test_reports.html)
    * [ ] code coverage computing and [integration](https://docs.gitlab.com/ee/ci/yaml/#coverage)
    * [ ] optimized [cache](https://docs.gitlab.com/ee/ci/caching/) configuration
* (optional) Code analysis job(s):
    * (type here the used code analysis tools)
    * [ ] mapped to the `test` stage
    * [ ] can be enabled/disabled by configuration
* (optional) Publish job:
    * (type here the used publish tools)
    * [ ] mapped to the `publish` stage
    * [ ] can be enabled by configuration
    * [ ] manually triggered on `master` branch

### Code Analysis template checklist

(Remove this chapter if not applicable to your template type.)

* Code analysis job:
    * [ ] mapped to the `test` stage
    * [ ] can be enabled/disabled by configuration
    * [ ] whenever possible, code analysis on non-`master`, non-`develop` branches should be a partial/light analysis
    * [ ] if the analysis is time consuming it shall be [triggered manually](https://docs.gitlab.com/ee/ci/yaml/#whenmanual)
      by default, and automatable by configuration

### Packaging template checklist

(Remove this chapter if not applicable to your template type.)

* [ ] Build job mapped to the `package-build` stage that build the package
* [ ] (optional) Code Analysis job(s) (lint, dependency check, ...) on the packaging descriptors and scripts
  mapped to the `build` or `test` stage
* [ ] (optional) Test job(s) mapped to the `package-test` stage that test and analyse the built package
* [ ] Publish job mapped to the `publish` stage that publishes the built package to a compatible package repository

### Deploy & Run template checklist

(Remove this chapter if not applicable to your template type.)

* Deployment jobs:
    * [ ] one hidden deploy job prototype
    * [ ] persist and propagate the `$CI_ENVIRONMENT_URL` variable as `environment_url` variable using a 
      [dotenv artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)
    * [ ] each env can be enabled/disabled by configuration
    * [ ] each env uses the [`resource_group`](https://docs.gitlab.com/ee/ci/yaml/#resource_group) feature to prevent 
      multiple pipelines from deploying to the same environment at the same time
    * [ ] **review** deployment job
        * mapped to the `deploy` stage
        * must be executed on non-`master`, non-`develop` branches only
        * must reference the **cleanup-review** job (see below) in its [`environment:on_stop`](https://docs.gitlab.com/ee/ci/yaml/#environmenton_stop)
    * [ ] **integration** deployment job
        * mapped to the `deploy` stage
        * must be executed on `develop` branch only
    * [ ] **staging** deployment job
        * mapped to the `deploy` stage
        * must be executed on `master` branch only
    * [ ] **production** deployment job
        * mapped to the `production` stage
        * must be executed on `master` branch only
* Cleanup jobs
    * [ ] one hidden cleanup job prototype
    * [ ] **review** cleanup job
        * mapped to the `deploy` stage
        * must be executed on non-`master`, non-`develop` branches only
        * must be associated to the [`environment:action:stop`](https://docs.gitlab.com/ee/ci/yaml/#environmentaction) event
* (optional) Analysis job(s) (linters, dependency checks, ...) depending on the technologies:
    * [ ] mapped to the `test` stage

### Acceptance template checklist

(Remove this chapter if not applicable to your template type.)

* Acceptance test job:
    * [ ] mapped to the `acceptance` stage
    * [ ] tests report integration using [JUnit test report](https://docs.gitlab.com/ee/ci/junit_test_reports.html)
    * [ ] auto-evaluating the environment url to test based on the possible upstream `$environment_url` variable or via 
      an `environment_url.txt` file.


## Contribution resources

Don't hesitate to review general [workflow rules](https://to-be-continuous.gitlab.io/doc/dev/workflow/)
and [coding guidelines](https://to-be-continuous.gitlab.io/doc/dev/guidelines/) for your contribution.


/label ~"kind/new-template"
